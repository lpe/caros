
\mainpage
\htmlinclude manifest.html

[TOC]

This component contains interfaces for communicating with various types of sensors. Examples hereof are camera sensors, RGB-D sensors, force/torque sensors and tactile array sensors.

