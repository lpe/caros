\mainpage

[TOC]

caros_common_robwork is a collection of RobWork oriented, or related, functionality that is used by the various CAROS nodes.

See [modules](modules.html) for the different categories.
