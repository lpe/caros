\mainpage

[TOC]

caros_common is a collection of common and convenience functionality that is used by the various CAROS nodes.

See [modules](modules.html) for the different categories. Especially see the @ref CAROS_NODE_SI for information regarding the structure of CAROS nodes.
