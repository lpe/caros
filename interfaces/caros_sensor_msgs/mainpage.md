
\mainpage
\htmlinclude manifest.html

[TOC]

This component contains ROS message types for various sensors, e.g. buttons and tactile arrays.